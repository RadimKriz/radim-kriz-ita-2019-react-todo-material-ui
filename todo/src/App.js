import React from 'react';
import Header from './Components/Header';
import Toddo from './Components/Toddo';
import RemoveBut from './Components/RemoveBut';
import './App.css';

class App extends React.Component {

   state = {
   newtodos: '',
   todos: ['']
           }

render() {
  return (
    <div style={{textAlign: "center"}}>

        <Header text="Todos App" />
 
        <form 
          onSubmit= {(event) => {
                                  event.preventDefault();
                                  this.setState({
                                              newtodos: "",
                                              todos: [...this.state.todos,
                                                       this.state.newtodos
                                                                          ]
                                                 })           
                                }}
        >
        <input 
        style = {{
          width: "40%",
          padding: "12px 20px",
          margin: "8px 0"
         }}
        placeholder ="What do you need to do?"
        value={this.state.newtodos}
        onChange = {
                (event) => {
                            this.setState({
                                          newtodos: event.target.value
                                          })
                            }}
      onPressEnter = {
                (event) => {
                           this.setState({
                                        newtodos: event.target.todos
                                        })
                            }}
    />
    <input 
       style = {{
        backgroundColor: "#4CAF50",
        border: "none",
        color: "white",
        padding: "16px 32px",
        textDecoration: "none",
        margin: "4px 2px",
        marginLeft: "10px",
        cursor: "pointer"
               }}
       type ="submit"
       value="New Todo"
    />
</form>
  


<Toddo todos={this.state.todos}

/>

<RemoveBut text ="Remove Todo"
          onClick = {(index, event) => {
                const value = this.state.todos
                value.splice(index, 1)    
                 this.setState({value})
           }}
/>

</div>

  );
}
}

export default App;
