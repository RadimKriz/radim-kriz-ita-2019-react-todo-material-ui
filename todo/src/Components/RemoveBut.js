import React from 'react';

const RemoveBut = (props) => {

    const mybuttonstyle =


     {

    backgroundColor: "#4CAF50",
    border: "none",
    color: "white",
    padding: "16px 32px",
    textDecoration: "none",
    fontSize: "16px",
    margin: "4px 2px",
    transitionDuration: "0.4",
    cursor: "pointer"

    }

  return (
     <button 
        style = {mybuttonstyle}
        onClick = {props.onClick}
     > 
        {props.text}
        
    </button>

)

}


export default RemoveBut;