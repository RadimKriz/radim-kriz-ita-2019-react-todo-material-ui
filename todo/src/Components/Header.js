import React from 'react'
import { EventEmitter } from 'events';


const Header = (props) => {

   const myheaderstyle =

    {
        fontFamily: "Arial",
        display: "flex",
        justifyContent: "center",
        padding: "10px",
        fontSize: "50px",
        fontWeight: "bold",
        letterSpacing: "3px",
        background: "linear-gradient(to right, #eee, #333)",
        color: "#2ECC40"
    };

  return (

      <h1 style={myheaderstyle}>{props.text}</h1>

         )

 }

export default Header