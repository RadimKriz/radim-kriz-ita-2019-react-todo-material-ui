import React from 'react';


const Toddos = (props) =>
{

 
const liststyle = {
    listStyleType: "none"
    }
  return (
         <ul style = {liststyle}>
           { 
            props.todos.map((item, index) => 
                <li key = {index}>
                    {item} 
                </li>)
           }

         </ul>    
         )

}


export default Toddos;